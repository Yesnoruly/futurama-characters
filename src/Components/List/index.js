import React from 'react';
import s from './List.module.scss'
import Item from "./Item";

const List = (props) => {
    return (
        <ul className={s.list}>
            <Item characters={props.characters} />
        </ul>
    )
}

export default List;