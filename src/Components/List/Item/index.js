import React from 'react';
import s from "./Item.module.scss";

const Item = (props) => {
    return (
        props.characters.map(c => {
            return (
                <li key={c.id} className={s.item}>
                    <div className={s.img}><img src={c.thumb} alt=""/></div>
                    <p className={s.name}><span>Name: </span>{c.name}</p>
                    <p className={s.company}><span>Company: </span>{c.company}</p>
                    <p className={s.species}><span>Species: </span>{c.species}</p>
                </li>
            )
        })
    )
}

export default Item;