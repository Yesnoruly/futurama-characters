import React from 'react';
import s from './SearchBar.module.scss'

const SearchBar = ({search, setSearch}) => {

    return (
            <div className="searchBar">
                <label
                    className={s.label}
                    htmlFor={"header-search"}>
                    <span>Search futurama characters</span>
                </label>
                <input
                    className={s.input}
                    type="text"
                    id="header-search"
                    placeholder="Search characters"
                    value={search}
                    onChange={e => setSearch(e.target.value)}
                />
            </div>
    )
}

export default SearchBar;