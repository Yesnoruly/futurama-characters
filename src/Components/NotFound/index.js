import React from 'react';
import s from './NotFound.module.scss'

const NotFound = () => {
    return (
        <div className={s.nfound}>
            The search for your request did not return any results. There may be a typo in your request.
        </div>
    )
}

export default NotFound;