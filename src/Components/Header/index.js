import React from 'react';

const Header = () => {
    return (
        <header className="header">
            <div className="container">
                <p>Futurama Characters</p>
            </div>
        </header>
    )
}

export default Header;