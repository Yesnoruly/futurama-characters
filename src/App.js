import React from 'react'
import './App.scss';
import Header from "./Components/Header";
import List from "./Components/List";
import SearchBar from "./Components/SearchBar";
import NotFound from "./Components/NotFound";

const App = (props) => {

    const [search, setSearch] = React.useState('')

    const filteredCharacters = search.length === 0 ? // search bar
        props.characters :
        props.characters.filter(character =>
            character.name.toLowerCase().includes(search.toLowerCase()) ||
            character.species.toLowerCase().includes(search.toLowerCase()) ||
            character.company.toLowerCase().includes(search.toLowerCase())
        )

    return (
        <div className="app">
            <Header />

            <main className="main">
                <div className="container">

                    <SearchBar
                        search={search}
                        setSearch={setSearch}
                    />
                    {filteredCharacters.length === 0 ? // If the array is empty
                        <NotFound /> :
                        <List characters={filteredCharacters} />
                    }
                </div>
            </main>
        </div>
    );
}

export default App;
