import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import characters from './characters.json';

ReactDOM.render(
  <React.StrictMode>
    <App characters={characters} />
  </React.StrictMode>,
  document.getElementById('root')
);
